# !! Wichtig !!
Das Plugin wird nicht weiter gepflegt.
Eine ähnliche Funktionalität kann seit Shop 5 auch mit dem OnPage Composer hergestellt werden. Dazu kann man beispielsweise das Product-Stream Portlet nutzen.

Die letzte Version des Plugins v2.0.0, kann bis einschließlich SHOP 5.2.2 genutzt werden.



# Nutzung

* Legen Sie in der WAWI das Kategorieattribut "show_on_frontpage" mit einem beliebigen numerischen Wert für die Kategorien an, die Sie auf Ihrer Startseite anzeigen möchten
* sortiert nach diesem Wert werden diese Kategorien auf der Startseite nach den weiteren Boxen wie "Top-Artikel", "Bestseller" oder "Neu im Sortiment" angezeigt 

# Changelog

## 2.0.0
* Shop5-Kompatibilität
* Smarty-Block-Erweiterung statt PHPQuery
